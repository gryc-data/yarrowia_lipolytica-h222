# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-05-18)

### Edited

* Build feature hierarchy.

### Fixed

* Refactor putative telomerase: YALIH222_S03E20648R.
* Wrong locus_tag value: YALIH222_S05E04390T/YALIH222_S05E04412G.
* Delete dulplicate misc_RNA feature: YALIH222_S06E04632R
* Delete a spurious mobile_element of 2 nucleotides: YALIH222_S07E04170T.
* CDS turned into pseudo: YALIH222_S01E10550G.
* CDS turned into pseudo: YALIH222_S02E25070T.
* CDS turned into pseudo: YALIH222_S03E05556T.
* CDS turned into pseudo: YALIH222_S10E01574T.
* CDS turned into pseudo: YALIH222_S01E22276G.
* Bad mRNA coordinates: YALIH222_S01E22782G.
* Bad mRNA and gene coordinates: YALIH222_S02E10462G.
* Bad mRNA and gene coordinates: YALIH222_S02E01200G.
* Bad mRNA coordinates: YALIH222_S02E29690G.
* Bad mRNA and gene coordinates: YALIH222_S02E08614G.
* Bad mRNA and gene coordinates: YALIH222_S03E24520G.
* Bad mRNA and gene coordinates: YALIH222_S03E10946G.
* Bad mRNA and gene coordinates: YALIH222_S03E31626G.
* Bad mRNA coordinates: YALIH222_S03E14950G.
* Bad mRNA and gene coordinates: YALIH222_S03E26060G.
* Bad mRNA and gene coordinates: YALIH222_S03E24498G.
* Bad mRNA and gene coordinates: YALIH222_S03E03136G.
* Bad mRNA and gene coordinates: YALIrm H222_S04E23024G.
* Bad mRNA and gene coordinates: YALIH222_S04E16182G.
* Bad mRNA and gene coordinates: YALIH222_S04E23002G.
* Bad mRNA and gene coordinates: YALIH222_S04E25268G.
* Bad mRNA and gene coordinates: YALIH222_S05E05820G.
* Bad mRNA and gene coordinates: YALIH222_S05E02938G.
* Bad mRNA and gene coordinates: YALIH222_S05E07426G.
* Bad mRNA and gene coordinates: YALIH222_S07E04126G.
* Bad mRNA and gene coordinates: YALIH222_S07E04104G.
* Bad mRNA and gene coordinates: YALIH222_S07E01024G.
* Bad mRNA and gene coordinates: YALIH222_S07E05116G.
* Bad mRNA and gene coordinates: YALIH222_S07E03422G.
* Bad mRNA and gene coordinates: YALIH222_S08E02410G.
* Bad mRNA and gene coordinates: YALIH222_S08E01486G.
* Bad mRNA and gene coordinates: YALIH222_S08E04808G.
* Bad mRNA and gene coordinates: YALIH222_S08E01552G.

## v1.0 (2021-05-18)

### Added

* The 17 annotated scaffolds of Yarrowia lipolytica H222 (source EBI, [GCA_900537225.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900537225.1)).
