## *Yarrowia lipolytica* H222

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB28424](https://www.ebi.ac.uk/ena/browser/view/PRJEB28424)
* **Assembly accession**: [GCA_900537225.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900537225.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Scaffold
* **Assembly name**: YALIH222
* **Assembly length**: 20,519,037
* **#Scaffolds**: 17
* **Mitochondiral**: No
* **N50 (L50)**: 3,955,271 (3)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 6545
* **Pseudogene count**: 130
* **tRNA count**: 526
* **rRNA count**: 106
* **Mobile element count**: 101
